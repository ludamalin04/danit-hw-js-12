'use strict'
let btns = document.querySelectorAll('.btn');

btns.forEach(function (item) {
    window.addEventListener('keyup', (target) => {
            if (target.key === item.textContent.toLowerCase() || target.key === item.textContent) {
                item.style.backgroundColor = 'blue';
            } else item.style.backgroundColor = '';
    })
})
